defmodule UrlShortenerWeb.PageControllerTest do
  use UrlShortenerWeb.ConnCase

  test "it outputs a shorter url than the one provided", %{conn: conn} do
    original_url = "https://www.example.com/a_very_long_url/provided_in_many_segments"

    conn = post(conn, "/", url: original_url)
    [ location | _ ] = get_resp_header(conn, "location")
    
    assert String.length(location) < String.length(original_url)
  end
end

defmodule UrlShortenerWeb.PageController do
  use UrlShortenerWeb, :controller

  alias UrlShortener 

  require Logger 
  
  def index(conn, %{"url" => url}) when is_binary(url) do
    with {:ok, url} <- UrlShortener.decode_url(url) do
      conn
      |> redirect(external: url)
    else 
      _ -> send_resp(conn, :not_found, [])
    end
  end

  def index(conn, _) do 
    with short_code <- get_session(conn, :short_code),
          conn <- delete_session(conn, :short_code) do
      conn
      |> render("index.html", code: short_code)
    end
  end

  def create(conn, %{"shortener" => %{"long_url" => url}}) do 
    with {:ok, short_url} <- UrlShortener.encode_url(url) do 
      conn
      |> put_resp_header("location", short_url) 
      |> put_session(:short_code, short_url)
      |> redirect(to: "/")
    end
  end
end

defmodule UrlShortenerWeb.Router do
  use UrlShortenerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :put_root_layout, {UrlShortenerWeb.LayoutView, :root}
    plug :fetch_session
    plug :fetch_flash
  end

  scope "/", UrlShortenerWeb do
    pipe_through :browser

    get "/:url", PageController, :index
    get "/", PageController, :index
    post "/", PageController, :create
  end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser
    end
  end
end

defmodule UrlShortener.Schema.Url do
  use Ecto.Schema
  import Ecto.Changeset

  schema "url" do
    field :long_url, :string

    timestamps(inserted_at: :created_at, 
                inserted_at_source: :created_at,
                updated_at: :last_used_at,
                updated_at_source: :last_used_at)
  end

  @doc false
  def changeset(url, attrs) do
    url
    |> cast(attrs, [:long_url])
    |> validate_required([:long_url])
  end
end

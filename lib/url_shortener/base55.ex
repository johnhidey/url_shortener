defmodule UrlShortener.Base55 do 
  use CustomBase, '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ'
  @moduledoc """
  Custom base numbering system for the encoding of the URL database id.  The 
  base of 62 would allow for all letters of the alphabet both lower and upper 
  case as well as the 10 digits.  To remove the ambaguity of different pairs
  of characters, the 0, i, l, o, I, L, O are removed giving a base 55 numbering
  system.
  """
end

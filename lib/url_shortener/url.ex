defmodule UrlShortener.Url do 
  alias UrlShortener.{Base55, Repo, Schema.Url}

  @type decode_return :: {:ok, url_type} 
                       | {:error, String.t}

  @type encode_return :: {:ok, encoded_url_type}
                       | {:error, String.t}

  @type url_type :: String.t

  @type encoded_url_type :: String.t

  @spec decode_url(encoded_url_type) :: decode_return
  def decode_url(url) do 
    with {:ok, id} <- Base55.decode(url),
          {:ok, url} <- get_url(id) do 
      {:ok, url.long_url}
    else 
      _ -> 
        {:error, "Decoding the short code errored"}
    end
  end

  @spec encode_url(url_type) :: encode_return
  def encode_url(url) do 
    with changeset <- Url.changeset(%Url{}, %{long_url: url}), 
          {:ok, url} <- Repo.insert(changeset), 
          url <- Base55.encode(url.id) do 
      {:ok, url}
    else
      _ -> 
        {:error, "Encoding the url failed"}
    end
  end

  @typep url_return :: {:ok, Schema.Url.t} 
                     | {:error, String.t}

  @spec get_url(pos_integer) :: url_return
  defp get_url(id) do 
    case Repo.get(Url, id) do 
      nil -> {:error, "Retrieving url errored"}
      url -> {:ok, url}
    end
  end
end

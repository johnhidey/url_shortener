defmodule UrlShortener do
  alias UrlShortener.Url

  @spec decode_url(String.t) :: Url.decode_return
  defdelegate decode_url(url), to: Url 

  @spec encode_url(String.t) :: Url.encode_return
  defdelegate encode_url(url), to: Url
end

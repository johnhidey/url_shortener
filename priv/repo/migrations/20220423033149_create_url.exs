defmodule UrlShortener.Repo.Migrations.CreateUrl do
  use Ecto.Migration

  def change do
    create table(:url) do
      add :long_url, :string

      timestamps(inserted_at: :created_at, 
                  inserted_at_source: :created_at,
                  updated_at: :last_used_at,
                  updated_at_source: :last_used_at)
    end

    execute("ALTER SEQUENCE url_id_seq RESTART WITH 1541418000001")
  end
end

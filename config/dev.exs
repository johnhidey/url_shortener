import Config

config :url_shortener, UrlShortener.Repo,
  username: "postgres",
  password: "postgres",
  database: "url_shortener_dev",
  hostname: "alpine",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :url_shortener, UrlShortenerWeb.Endpoint,
  http: [ip: {0, 0, 0, 0}, port: 4000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/without_live_web/(live|views)/.*(ex)$",
      ~r"lib/without_live_web/templates/.*(eex)$"
    ]
  ], 
  secret_key_base: "2tANe4ZtX9bWrWKwHNW6wXJltZhiE+ouiZeX4aAa7ShnxMkdFfZzJCrZYm0Crisc",
  watchers: [
    esbuild: {Esbuild, :install_and_run, [:default, ~w(--sourcemap=inline --watch)]}
  ]

config :logger, :console, format: "[$level] $message\n"
config :phoenix, :stacktrace_depth, 20
config :phoenix, :plug_init_mode, :runtime

import Config
config :url_shortener, UrlShortener.Repo,
  username: "postgres",
  password: "postgres",
  database: "url_shortener_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "alpine",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :url_shortener, UrlShortenerWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "zoAJTRxlcgJa/EXQ82h+PMwf8CQQksQglMCsib/AnKM8JRWWTLtp4BPe17wTaz6U",
  server: false

config :logger, level: :warn
config :phoenix, :plug_init_mode, :runtime

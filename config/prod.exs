import Config

config :url_shortener, UrlShortenerWeb.Endpoint,
  url: [host: "localhost", port: 8000],
  cache_static_manifest: "priv/static/cache_manifest.json"

config :logger, level: :info
